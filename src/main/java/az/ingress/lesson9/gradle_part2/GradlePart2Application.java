package az.ingress.lesson9.gradle_part2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GradlePart2Application {

	public static void main(String[] args) {
		SpringApplication.run(GradlePart2Application.class, args);
	}

}
